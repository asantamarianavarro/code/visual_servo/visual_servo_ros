// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _artag_pbvs_alg_node_h_
#define _artag_pbvs_alg_node_h_

#include <iri_base_algorithm/iri_base_algorithm.h>
#include "artag_pbvs_alg.h"

// [publisher subscriber headers]
#include <std_msgs/Float64MultiArray.h>
#include <visualization_msgs/MarkerArray.h>
#include <ar_tools_msgs/ARMarkers.h>
#include <geometry_msgs/TwistWithCovariance.h>

// [service client headers]

// [action server client headers]
#include <iri_action_server/iri_action_server.h>
#include <iri_visual_servo_msgs/vs_targetAction.h>

using namespace Eigen;
using namespace std;


/**
 * \brief IRI ROS Specific Algorithm Class
 *
 */
class ArtagPbvsAlgNode : public algorithm_base::IriBaseAlgorithm<ArtagPbvsAlgorithm>
{
  private:
    // [publisher attributes]
    ros::Publisher debug_data_publisher_;
    std_msgs::Float64MultiArray debug_data_msg_;

    ros::Publisher marker_array_publisher_;
    visualization_msgs::MarkerArray MarkerArray_msg_;
    ros::Publisher cmd_vel_cov_publisher_;
    geometry_msgs::TwistWithCovariance TwistWithCovariance_msg_;

    // [subscriber attributes]
    ros::Subscriber input_ARtag_subscriber_;
    void input_ARtag_callback(const ar_tools_msgs::ARMarkers::ConstPtr& msg);
    CMutex input_ARtag_mutex_;

    MatrixXd cam_vel_; //Camera velocities.
    MatrixXd error_; // Pose error (PBVS) between desired and current poses.

    ros::Time time_,time_last_,time_ini_; //Time variables.
    double dt_; // Time diferential.

    ArtagPbvsAlgorithm artag_pbvs_alg_;   // Algorithm call.

    // ONly Image-Based parameters
    MatrixXd v_rollpitch_; // Roll and pitch quadrotor angular velocities (to extract from output velocities).
    bool init_; // To initialize time counters.
    MatrixXd features_; // Virtual features detected from the object.
    MatrixXd base_; // Base of the object's virtual features.

    MatrixXd desired_pose_; // Desired camera position.
    MatrixXd current_pose_; // Current camera position.
    MatrixXd kp_, kd_, ki_; // PID parameters.
    double i_lim_;  // Integration limits (+ and -).
    double cov_;

    double fixed_to_id_; //Anchored to this ID
    geometry_msgs::Pose pose_; //tag pose
    double curr_dist_error_; // Current euclidean linear distance to goal
    double curr_angle_error_; // Current euclidean angular distance to goal

    bool enable_; // Enable the visual servo. By default is enabled.
    bool marker_ok_; // Maker event.
    MatrixXd max_confidence_error_; // Max confidence error (Euclidean linear and agular distances)
    bool is_in_zone_; // If the robot is in the confidence zone
    bool is_in_dist_zone_; // If the robot is inside the linear euclidean confidence distance
    bool is_in_angular_zone_; // If the robot is inside the angular euclidean confidence distance


    string frame_id_; // Tag frame ID
    double first_dist_; // Initial distance to tag
    bool first_time_; // First action execution time
    bool first_tag_; // First tag arrival

    bool write_files_; // Enable write some vars to files to plot them externally
    string folder_name_; // Folder name to write the files
    double curr_t_; // Current execution time

    std::vector<float> debug_data_; // Vector to publish debug data

    // [service attributes]

    // [client attributes]

    // [action server attributes]
    IriActionServer<iri_visual_servo_msgs::vs_targetAction> target_aserver_;
    void targetStartCallback(const iri_visual_servo_msgs::vs_targetGoalConstPtr& goal);
    void targetStopCallback(void);
    bool targetIsFinishedCallback(void);
    bool targetHasSucceedCallback(void);
    void targetGetResultCallback(iri_visual_servo_msgs::vs_targetResultPtr& result);
    void targetGetFeedbackCallback(iri_visual_servo_msgs::vs_targetFeedbackPtr& feedback);

    // [action client attributes]

   /**
    * \brief Broadcast tf
    *
    * Broadcast tf
    */
    void broadcast_tf(const  Matrix4d& eigenT, const std::string& parent, const std::string& child);

   /**
    * \brief Broadcast tf
    *
    * Broadcast tf
    */
    void broadcast_tf(const  tf::Transform& transform, const std::string& parent, const std::string& child);

   /**
    * \brief Get transform matrix
    *
    * From Eigen 4x4 to tf homogeneous transform
    */
    void eigen_to_tf(const  Matrix4d& eigenT, tf::Transform& transform);

    /**
    * \brief Get Euler RPY from Homogenous transform
    *
    */
    Vector3d R_to_rpy(const  Matrix4d& eigenT);

    /**
    * \brief Get Euler RPY from Rotation matrix
    *
    */
    Vector3d R_to_rpy(const  Matrix3d& R);
  public:
   /**
    * \brief Constructor
    * 
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
    ArtagPbvsAlgNode(void);

   /**
    * \brief Destructor
    * 
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~ArtagPbvsAlgNode(void);

  protected:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency 
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects 
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread(void);

   /**
    * \brief Check if the camera is Inside the confidence marker zone
    *
    * Check if the camera is Inside the confidence marker zone defined by the user
    */    
    void inside_confidence_zone();


   /**
    * \brief Get marker info
    * 
    * Extract marker data: time, pose and ID,
    */
    void get_marker_info(const ar_tools_msgs::ARMarkers::ConstPtr& msg);

   /**
    * \brief Set angles between -pi and pi
    *
    * Set angles between -pi and pi
    */   
    double pi2pi(double& angle);

   /**
    * \brief Fill markers message parameters
    *
    * Fill markers message parameters
    */   
    void fill_marker_array();

    /**
     * \brief Fill Goal marker message
     *
     * Fill Goal marker message
     */
    visualization_msgs::Marker fill_goal_marker();

   /**
    * \brief Fill Toroid marker message
    *
    * Fill Toroid marker message
    */  
    visualization_msgs::Marker fill_toroid_marker();

   /**
    * \brief Fill Euclidean Linear distance text marker message
    *
    * Fill Euclidean Linear distance text marker message
    */  
    visualization_msgs::Marker fill_m_dist_text();

   /**
    * \brief Fill Euclidean Angular distance text marker message
    *
    * Fill Euclidean Angular distance text marker message
    */  
    visualization_msgs::Marker fill_m_angle_text();

   /**
    * \brief Fill Image-Based Virtual Features marker messages
    *
    * Fill Image-Based Virtual Features marker messages
    */ 
    visualization_msgs::MarkerArray fill_m_features();

   /**
    * \brief Fill Image-Based Virtual Base marker messages
    *
    * Fill Image-Based Virtual Base marker messages
    */ 
    visualization_msgs::MarkerArray fill_m_base();

   /**
    * \brief Get Homogenous Transformation
    *
    * Get Homogenous Transformation from x,y,z,roll,pitch,yaw
    */ 
    Matrix4d get_HT(const MatrixXd& pose);

   /**
    * \brief dynamic reconfigure server callback
    * 
    * This method is called whenever a new configuration is received through
    * the dynamic reconfigure. The derivated generic algorithm class must 
    * implement it.
    *
    * \param config an object with new configuration from all algorithm 
    *               parameters defined in the config file.
    * \param level  integer referring the level in which the configuration
    *               has been changed.
    */
    void node_config_update(Config &config, uint32_t level);

   /**
    * \brief node add diagnostics
    *
    * In this abstract function additional ROS diagnostics applied to the 
    * specific algorithms may be added.
    */
    void addNodeDiagnostics(void);

    // [diagnostic functions]
    
    // [test functions]
};

#endif
