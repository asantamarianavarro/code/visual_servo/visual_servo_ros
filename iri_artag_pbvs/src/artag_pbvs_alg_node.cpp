#include "artag_pbvs_alg_node.h"

using namespace Eigen;
using namespace std;

//! Class constructor
ArtagPbvsAlgNode::ArtagPbvsAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<ArtagPbvsAlgorithm>(),
  target_aserver_(public_node_handle_, "target")
{
  //init class attributes if necessary
  //this->loop_rate_ = 100;//in [Hz]

  this->curr_t_ = 0;

  this->time_ini_ = ros::Time::now();

  this->enable_=false;
  this->marker_ok_ = false;
  this->cov_ = 100;
  this->fixed_to_id_ = 0;
  this->is_in_zone_=false;
  this->is_in_dist_zone_ = false;
  this->is_in_angular_zone_ = false;

  this->MarkerArray_msg_.markers.reserve(100);

  this->public_node_handle_.param<bool>("write_files_", this->write_files_, false);
  this->public_node_handle_.param<string>("folder_name_", this->folder_name_, "/Desktop/Results");

  //Initialize vectors
  this->current_pose_ = MatrixXd::Zero(6,1);
  this->desired_pose_ = MatrixXd::Zero(6,1);
  this->kp_ = MatrixXd::Zero(6,1);
  this->kd_ = MatrixXd::Zero(6,1);
  this->ki_ = MatrixXd::Zero(6,1);
  this->cam_vel_ = MatrixXd::Zero(6,1);
  this->error_ = MatrixXd::Zero(6,1);
  this->max_confidence_error_ = MatrixXd::Zero(2,1);

  this->init_=true; 

  // Get desired position from launch file
  double x,y,z,roll_tmp,roll,pitch,yaw;
  this->public_node_handle_.param<double>("desired_x", x, 0);
  this->public_node_handle_.param<double>("desired_y", y, 0);
  this->public_node_handle_.param<double>("desired_z", z, 0);
  this->public_node_handle_.param<double>("desired_roll", roll_tmp, 0);
  this->public_node_handle_.param<double>("desired_pitch", pitch, 0);
  this->public_node_handle_.param<double>("desired_yaw", yaw, 0);
  roll = roll_tmp-3.14159265359;

  // Confidence zone
  this->public_node_handle_.param<double>("confidence_linear", this->max_confidence_error_(0,0), 0);
  this->public_node_handle_.param<double>("confidence_angular", this->max_confidence_error_(1,0), 0);
  
  // High value initialization (robot far from goal)
  this->curr_dist_error_ = 100;
  this->curr_angle_error_ = 100;
  this->first_dist_ = this->curr_dist_error_;
  this->first_time_ = false;
  this->first_tag_ = false;

  this->desired_pose_(0,0) = x;
  this->desired_pose_(1,0) = y;
  this->desired_pose_(2,0) = z;
  this->desired_pose_(3,0) = pi2pi(roll); 
  this->desired_pose_(4,0) = pi2pi(pitch);
  this->desired_pose_(5,0) = pi2pi(yaw);

  // [init publishers]
  this->debug_data_publisher_ = this->public_node_handle_.advertise<std_msgs::Float64MultiArray>("debug_data", 1);
  this->marker_array_publisher_ = this->public_node_handle_.advertise<visualization_msgs::MarkerArray>("marker_array", 10);
  this->cmd_vel_cov_publisher_ = this->public_node_handle_.advertise<geometry_msgs::TwistWithCovariance>("cmd_vel_cov", 10);
  
  // [init subscribers]
  this->input_ARtag_subscriber_ = this->public_node_handle_.subscribe("input_ARtag", 10, &ArtagPbvsAlgNode::input_ARtag_callback, this);
  
  // [init services]
  
  // [init clients]
  
  // [init action servers]
  target_aserver_.registerStartCallback(boost::bind(&ArtagPbvsAlgNode::targetStartCallback, this, _1)); 
  target_aserver_.registerStopCallback(boost::bind(&ArtagPbvsAlgNode::targetStopCallback, this)); 
  target_aserver_.registerIsFinishedCallback(boost::bind(&ArtagPbvsAlgNode::targetIsFinishedCallback, this)); 
  target_aserver_.registerHasSucceedCallback(boost::bind(&ArtagPbvsAlgNode::targetHasSucceedCallback, this)); 
  target_aserver_.registerGetResultCallback(boost::bind(&ArtagPbvsAlgNode::targetGetResultCallback, this, _1)); 
  target_aserver_.registerGetFeedbackCallback(boost::bind(&ArtagPbvsAlgNode::targetGetFeedbackCallback, this, _1)); 
  target_aserver_.start();
  
  // [init action clients]
}

//! Class destructor
ArtagPbvsAlgNode::~ArtagPbvsAlgNode(void)
{
  // [free dynamic memory]
}

//! Main node thread
void ArtagPbvsAlgNode::mainNodeThread(void)
{

  // [fill msg structures]
  // Initialize the topic message structure
  //this->debug_data_msg_.data = my_var;

  //this->marker_array_MarkerArray_msg_.data = my_var;

  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]

  // [publish messages]
}

/*  [subscriber callbacks] */

//! Input ARtag callback
void ArtagPbvsAlgNode::input_ARtag_callback(const ar_tools_msgs::ARMarkers::ConstPtr& msg) 
{ 
  //ROS_INFO("ArtagPbvsAlgNode::input_ARtag_callback: New Message Received"); 

  //use appropiate mutex to shared variables if necessary 
  this->input_ARtag_mutex_.enter(); 

  this->cam_vel_ = MatrixXd::Zero(6,1);
 
  if (msg->markers.empty()){
  
    this->marker_ok_ = false;

    //Covariance matrix set to high values because there is no marker
    this->TwistWithCovariance_msg_.covariance[0]  = 
    this->TwistWithCovariance_msg_.covariance[7]  = 
    this->TwistWithCovariance_msg_.covariance[14] = 
    this->TwistWithCovariance_msg_.covariance[21] = 
    this->TwistWithCovariance_msg_.covariance[28] = 
    this->TwistWithCovariance_msg_.covariance[35] = 99999;

  }
  else if (!msg->markers.empty() && this->enable_){
  
    this->marker_ok_ = true;

    this->time_last_ = this->time_;

    this->alg_.lock();      

    // markers check
    if ((!isnan(msg->markers[0].pose.pose.orientation.x) && isfinite(msg->markers[0].pose.pose.orientation.x)) || (!isnan(msg->markers[1].pose.pose.orientation.x) && isfinite(msg->markers[1].pose.pose.orientation.x)))
    {
      // Get marker info
      get_marker_info(msg);

      this->cov_ = double(100-msg->markers.back().confidence)/100;
    }
    else ROS_ERROR("Marker Reading error");

    this->alg_.unlock();

    // Avoid detection ambiguities with z axis check  
    if (this->current_pose_(2,0) > 0)
    {
      // Convert desired position in tag to camera frame
      Matrix4d HT_tagincam = get_HT(this->current_pose_);
      Matrix4d HT_desintag = get_HT(this->desired_pose_);
      Matrix4d HT_desincam = HT_tagincam * HT_desintag;
      MatrixXd desired_cam = MatrixXd::Zero(6,1);
      desired_cam.block(0,0,3,1) = HT_desincam.block(0,3,3,1);
      desired_cam.block(3,0,3,1) = R_to_rpy(HT_desincam);

      // DEBUG
      //broadcast_tf(HT_tagincam,"/kinton/bottom_cam_optical_frame","/debug_tag");
      //broadcast_tf(HT_desincam,"/kinton/bottom_cam_optical_frame","/debug_target");
      broadcast_tf(HT_desintag,"/95","/target");

      // this->curr_t_ = this->curr_t_ + this->dt_;
      this->curr_t_ = (ros::Time::now()-this->time_ini_).toSec();

      // Pose-Base Visual Servo algorithm
      this->artag_pbvs_alg_.pose_based_vs(desired_cam, 
        MatrixXd::Zero(6,1),this->dt_,
        this->kp_,this->kd_,this->ki_,
        this->i_lim_,this->write_files_,this->folder_name_,
        this->curr_t_,this->error_,this->cam_vel_,this->debug_data_);

      // Publish debug data
      this->debug_data_msg_.layout.dim.resize(1);
      this->debug_data_msg_.layout.dim[0].label = "";
      this->debug_data_msg_.layout.dim[0].size = this->debug_data_.size();
      this->debug_data_msg_.layout.dim[0].stride = this->debug_data_.size();
      this->debug_data_msg_.layout.data_offset = 0;

      this->debug_data_msg_.data.resize(this->debug_data_.size());
      for (int ii = 0; ii < this->debug_data_.size(); ++ii)
        this->debug_data_msg_.data[ii] = this->debug_data_.at(ii);
      this->debug_data_publisher_.publish(this->debug_data_msg_);
    }  

    //Covariance matrix set to low values because there is marker, and 
    this->TwistWithCovariance_msg_.covariance[0]  = 
    this->TwistWithCovariance_msg_.covariance[7]  = 
    this->TwistWithCovariance_msg_.covariance[14] = 
    this->TwistWithCovariance_msg_.covariance[21] = 
    this->TwistWithCovariance_msg_.covariance[28] = 
    this->TwistWithCovariance_msg_.covariance[35] = this->cov_;

  }

  this->first_tag_ = true;

  // Check if finished
  inside_confidence_zone();

  // Fill marker
  fill_marker_array();

  //Twist message to publish with the camera velocities
  this->TwistWithCovariance_msg_.twist.linear.x=this->cam_vel_(0,0);
  this->TwistWithCovariance_msg_.twist.linear.y=this->cam_vel_(1,0);
  this->TwistWithCovariance_msg_.twist.linear.z=this->cam_vel_(2,0);
  this->TwistWithCovariance_msg_.twist.angular.x=this->cam_vel_(3,0);
  this->TwistWithCovariance_msg_.twist.angular.y=this->cam_vel_(4,0);
  this->TwistWithCovariance_msg_.twist.angular.z=this->cam_vel_(5,0);
  
  this->cmd_vel_cov_publisher_.publish(this->TwistWithCovariance_msg_);

  //unlock previously blocked shared variables 
  this->input_ARtag_mutex_.exit();
}

//! Set angles between -pi and pi
void ArtagPbvsAlgNode::inside_confidence_zone()
{
  double x,y,z,roll,pitch,yaw;

  this->is_in_zone_ = false;
  this->is_in_dist_zone_ = false;
  this->is_in_angular_zone_ = false;

  x = this->desired_pose_(0,0)-this->current_pose_(0,0);
  y = this->desired_pose_(1,0)-this->current_pose_(1,0);
  z = this->desired_pose_(2,0)-this->current_pose_(2,0);
  roll = this->desired_pose_(3,0)-this->current_pose_(3,0);
  pitch = this->desired_pose_(4,0)-this->current_pose_(4,0);
  yaw = this->desired_pose_(5,0)-this->current_pose_(5,0);

  roll = pi2pi(roll);
  pitch = pi2pi(pitch);
  yaw = pi2pi(yaw);

  this->curr_dist_error_ = sqrt(x*x+y*y+z*z);
  double angle_err = sqrt(roll*roll+pitch*pitch+yaw*yaw);
  this->curr_angle_error_ = abs(pi2pi(angle_err));

  if(this->curr_dist_error_ < this->max_confidence_error_(0,0))
    this->is_in_dist_zone_=true;

  if(this->curr_angle_error_ < this->max_confidence_error_(1,0))
    this->is_in_angular_zone_=true;

  if(this->is_in_dist_zone_ && this->is_in_angular_zone_)
    this->is_in_zone_ = true;
}

//! Get Marker info
void ArtagPbvsAlgNode::get_marker_info(const ar_tools_msgs::ARMarkers::ConstPtr& msg)
{

  this->pose_ = msg->markers.back().pose.pose;
  this->time_ = msg->markers.back().header.stamp;
  this->frame_id_ = msg->markers.back().header.frame_id;


  //reset time that will reset the algorithm integration
  if (this->init_)
  {
    this->time_last_=this->time_;
    this->init_=false;
  }
  this->dt_=(this->time_-this->time_last_).toSec();  

  // Get pose
  tf::Transform tf_pose;
  Matrix3d R;
  double roll,pitch,yaw;
  tf::poseMsgToTF(this->pose_, tf_pose);
  tf_pose.getBasis().getRPY(roll,pitch,yaw);

  this->current_pose_(0,0) = this->pose_.position.x; 
  this->current_pose_(1,0) = this->pose_.position.y; 
  this->current_pose_(2,0) = this->pose_.position.z;
  this->current_pose_(3,0) = roll;
  this->current_pose_(4,0) = pitch;
  this->current_pose_(5,0) = yaw;
}

//! Set angles between -pi and pi
double ArtagPbvsAlgNode::pi2pi(double& angle)
{
	double pi = 3.14159265359;
  while (angle >= pi) angle = angle-2*pi;
  while (angle <= -pi) angle = angle+2*pi;
  return angle;
}

void ArtagPbvsAlgNode::fill_marker_array()
{

  if(this->first_tag_)
  {

    if (this->first_time_)
    {
      this->first_dist_ = this->curr_dist_error_;
      this->first_time_ = false;
    }

    this->MarkerArray_msg_.markers.clear();

    visualization_msgs::Marker toroid_marker,m_dist_text,m_angle_text,m_goal;

    m_goal = fill_goal_marker();
    this->MarkerArray_msg_.markers.push_back(m_goal);

    toroid_marker = fill_toroid_marker();
    this->MarkerArray_msg_.markers.push_back(toroid_marker);

    m_dist_text = fill_m_dist_text();
    this->MarkerArray_msg_.markers.push_back(m_dist_text);

    m_angle_text = fill_m_angle_text();
    this->MarkerArray_msg_.markers.push_back(m_angle_text);

    this->marker_array_publisher_.publish(this->MarkerArray_msg_);
  }
}

visualization_msgs::Marker ArtagPbvsAlgNode::fill_goal_marker()
{
  double red = (this->curr_dist_error_/this->first_dist_);
  double green = 1-red;
	Eigen::MatrixXd of_pose = this->current_pose_ - this->desired_pose_;

  // Torid Marker (mesh resource)
	visualization_msgs::Marker current_marker;
  current_marker.header.frame_id = this->frame_id_;
  current_marker.header.stamp = this->time_;
  current_marker.ns = "basic_shapes";
  current_marker.type = visualization_msgs::Marker::SPHERE;
  current_marker.action = visualization_msgs::Marker::ADD;
  current_marker.lifetime = ros::Duration(2*this->dt_);
  current_marker.id = 1;

  geometry_msgs::Pose pose;
  pose.position.x = of_pose(0,0);
  pose.position.y = of_pose(1,0);
	pose.position.z = of_pose(2,0);
	tf::Quaternion q;
	q.setRPY(of_pose(3,0),of_pose(3,0),of_pose(3,0));
	tf::quaternionTFToMsg(q, pose.orientation);
  current_marker.pose = pose;
  current_marker.scale.x = 0.025;
  current_marker.scale.y = 0.025;
  current_marker.scale.z = 0.025;
  current_marker.color.r = (float)red;
  current_marker.color.g = (float)green;
  current_marker.color.b = 0.0f;
  current_marker.color.a = 1.0;

  return current_marker;
}

visualization_msgs::Marker ArtagPbvsAlgNode::fill_toroid_marker()
{
  // Torid Marker (mesh resource)
  visualization_msgs::Marker current_marker;
  double red = (this->curr_dist_error_/this->first_dist_);
  double green = 1-red;    

  current_marker.header.frame_id = this->frame_id_;
  current_marker.header.stamp = this->time_;
  current_marker.ns = "basic_shapes"; 
  current_marker.type = visualization_msgs::Marker::MESH_RESOURCE;
  current_marker.mesh_resource = "package://iri_artag_pbvs/media/models/target_marker.stl";
  current_marker.action = visualization_msgs::Marker::ADD;
  current_marker.lifetime = ros::Duration(2*this->dt_);
  current_marker.id = 0;
  current_marker.pose = this->pose_;
  current_marker.scale.x = 0.075;
  current_marker.scale.y = 0.075;
  current_marker.scale.z = 0.075;
  current_marker.color.a = 1.0;

  if (this->is_in_zone_) current_marker.color.a = 0.25; 
  current_marker.color.r = (float)red;
  current_marker.color.g = (float)green;
  current_marker.color.b = 0.0f;

  return current_marker;
}

visualization_msgs::Marker ArtagPbvsAlgNode::fill_m_dist_text()
{
  // Text marker Linear Euclidean Distance
  visualization_msgs::Marker m_dist_text;
  m_dist_text.header.frame_id = this->frame_id_;
  m_dist_text.header.stamp = this->time_;
  m_dist_text.ns = "basic_shapes";
  m_dist_text.id = 2;
  m_dist_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  m_dist_text.action = visualization_msgs::Marker::ADD;
  m_dist_text.pose = this->pose_;
  m_dist_text.pose.position.x += 0.5;
  m_dist_text.pose.position.y += 0.5;
  m_dist_text.scale.x = 0;
  m_dist_text.scale.y = 0;
  m_dist_text.scale.z = 0.25;
  m_dist_text.color.a = 1.0;
  if (this->is_in_dist_zone_)
  {
    m_dist_text.color.r = 0.0;
    m_dist_text.color.g = 1.0;
    m_dist_text.color.b = 0.0;
  }
  else 
  {
    m_dist_text.color.r = 1.0;
    m_dist_text.color.g = 0.0;
    m_dist_text.color.b = 0.0;      
  }
  ostringstream dist_strs;
  dist_strs << "L:";
  dist_strs << this->curr_dist_error_;
  string dist_text = dist_strs.str();
  m_dist_text.text=dist_text;

  return m_dist_text;
}

visualization_msgs::Marker ArtagPbvsAlgNode::fill_m_angle_text()
{
  // Text marker Linear Euclidean Distance
  visualization_msgs::Marker m_angle_text;
  m_angle_text.header.frame_id = this->frame_id_;
  m_angle_text.header.stamp = this->time_;
  m_angle_text.ns = "basic_shapes";
  m_angle_text.id = 3;
  m_angle_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  m_angle_text.action = visualization_msgs::Marker::ADD;
  m_angle_text.pose = this->pose_;
  m_angle_text.pose.position.x += 0.7;
  m_angle_text.pose.position.y += 0.7;
  m_angle_text.scale.x = 0;
  m_angle_text.scale.y = 0;
  m_angle_text.scale.z = 0.25;
  m_angle_text.color.a = 1.0;
  if (this->is_in_angular_zone_)
  {
    m_angle_text.color.r = 0.0;
    m_angle_text.color.g = 1.0;
    m_angle_text.color.b = 0.0;
  }
  else 
  {
    m_angle_text.color.r = 1.0;
    m_angle_text.color.g = 0.0;
    m_angle_text.color.b = 0.0;      
  }
  ostringstream angular_strs;
  angular_strs << "A:";
  angular_strs << this->curr_angle_error_;
  string angle_text = angular_strs.str();
  m_angle_text.text=angle_text;  

  return m_angle_text;
}

visualization_msgs::MarkerArray ArtagPbvsAlgNode::fill_m_features()
{
  visualization_msgs::MarkerArray m_features;

  visualization_msgs::Marker marker;
  marker.header.frame_id = this->frame_id_;
  marker.header.stamp = this->time_;
  marker.ns = "basic_shapes";
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  // marker.pose = this->pose_;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.05;
  marker.scale.y = 0.05;
  marker.scale.z = 0.05;
  marker.color.a = 1.0;
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.75;

  Matrix4d HT_target = get_HT(this->current_pose_);
  MatrixXd point;
  for (int ii = 0; ii < this->features_.cols(); ++ii)
  {
    point = HT_target * this->features_.col(ii);

    marker.pose.position.x = point(0,0);
    marker.pose.position.y = point(1,0);
    marker.pose.position.z = point(2,0);
    marker.id = 4+ii;
    m_features.markers.push_back(marker);
  }

  return m_features;
}

visualization_msgs::MarkerArray ArtagPbvsAlgNode::fill_m_base()
{
  visualization_msgs::MarkerArray m_base;
 
  visualization_msgs::Marker marker;
  marker.header.frame_id = this->frame_id_;
  marker.header.stamp = this->time_;
  marker.ns = "basic_shapes";
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  // marker.pose = this->pose_;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;  
  marker.scale.x = 0.05;
  marker.scale.y = 0.05;
  marker.scale.z = 0.05;
  marker.color.a = 1.0;
  marker.color.r = 0.75;
  marker.color.g = 0.0;
  marker.color.b = 1.0;

  Matrix4d HT_target = get_HT(this->current_pose_);
  MatrixXd point;
  for (int ii = 0; ii < this->base_.cols(); ++ii)
  {
    point = HT_target * this->base_.col(ii);
    marker.pose.position.x = point(0,0);
    marker.pose.position.y = point(1,0);
    marker.pose.position.z = point(2,0);
    marker.id = 4+this->features_.cols()+ii;
    m_base.markers.push_back(marker);
  }
  return m_base;
}

Matrix4d ArtagPbvsAlgNode::get_HT(const MatrixXd& pose)
{
  // euler convention zyx
  Eigen::Matrix3d Rot; 
  Rot = Eigen::AngleAxisd(pose(5,0), Eigen::Vector3d::UnitZ()) \
    * Eigen::AngleAxisd(pose(4,0), Eigen::Vector3d::UnitY()) \
    * Eigen::AngleAxisd(pose(3,0), Eigen::Vector3d::UnitX());    
   
  Eigen::Matrix4d HT(4,4);
  HT.block(0,0,3,3)=Rot;  
  HT.block(0,3,3,1) << pose(0,0), pose(1,0), pose(2,0);
  HT.row(3) << 0.0, 0.0, 0.0, 1.0;

  return HT;
}

void ArtagPbvsAlgNode::broadcast_tf(const  Matrix4d& eigenT, const std::string& parent, const std::string& child)
{
  tf::Transform transform;
  eigen_to_tf(eigenT,transform);
  broadcast_tf(transform,parent,child);
}
void ArtagPbvsAlgNode::broadcast_tf(const  tf::Transform& transform, const std::string& parent, const std::string& child)
{
  static tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parent, child));
}
void ArtagPbvsAlgNode::eigen_to_tf(const  Matrix4d& eigenT, tf::Transform& transform)
{
  double x,y,z,roll,pitch,yaw;
  x = eigenT(0,3);
  y = eigenT(1,3);
  z = eigenT(2,3);
  Matrix3d Rot = eigenT.block(0,0,3,3);
  Vector3d angles = Rot.eulerAngles(2, 1, 0);

  transform.setOrigin( tf::Vector3(x, y, z) );
  tf::Quaternion q;
  q.setRPY(angles(2,0),angles(1,0),angles(0,0));
  transform.setRotation(q);
}

Vector3d ArtagPbvsAlgNode::R_to_rpy(const  Matrix4d& eigenT)
{
  Vector3d angles;
  Matrix3d R = eigenT.block(0,0,3,3);
  angles = R_to_rpy(R);
  return angles;
}
Vector3d ArtagPbvsAlgNode::R_to_rpy(const  Matrix3d& R)
{
  // based on http://staff.city.ac.uk/~sbbh653/publications/euler.pdf
  Vector3d angles;
  double pi = 3.14159265358979323846;

  if (R(2,0)!=1 && R(2,0)!=-1)
  {
    // First solution
    double r1,r2,p1,p2,y1,y2;
    r1 = - std::asin(R(2,0));
    r2 = pi - r1;
    p1 = std::atan2(R(2,1)/cos(r1),R(2,2)/cos(r1));
    p2 = std::atan2(R(2,1)/cos(r2),R(2,2)/cos(r2));
    y1 = std::atan2(R(1,0)/cos(r1),R(0,0)/cos(r1));
    y2 = std::atan2(R(1,0)/cos(r2),R(0,0)/cos(r2));

    // Choose the closest angles to zero
    if ((std::abs(r1)+std::abs(r1)+std::abs(r1)) < (std::abs(r2)+std::abs(r2)+std::abs(r2)))
    {
      angles(0,0) = r1;
      angles(1,0) = p1;
      angles(2,0) = y1;
    }
    else
    {
      angles(0,0) = r2;
      angles(1,0) = p2;
      angles(2,0) = y2;
    }
  }
  else
  {
    angles(0,0) = 0.0;
    if (R(2,0)==-1)
    {
      angles(1,0) = pi/2;
      angles(2,0) = angles(0,0) + std::atan2(R(0,1),R(0,3));
    }  
    else
    {
      angles(1,0) = -pi/2;
      angles(2,0) = -angles(0,0) + std::atan2(-R(0,1),-R(0,3));      
    }
  }

  return angles;
}
/*  [service callbacks] */

/*  [action callbacks] */
void ArtagPbvsAlgNode::targetStartCallback(const iri_visual_servo_msgs::vs_targetGoalConstPtr& goal)
{ 
  alg_.lock();

    //check goal 
    this->enable_ = true;
    this->init_ = true;
    this->is_in_zone_ = false;

    this->fixed_to_id_ = (double)goal->id;
    for (int ii = 0; ii < 6; ++ii)
    {
      this->desired_pose_(ii,0) = goal->desired_pose[ii];
    }
    for (int ii = 0; ii < 2; ++ii)
    {
      this->max_confidence_error_(ii,0) = goal->max_confidence_error[ii];
    }

    this->first_time_ = true;
    this->first_tag_ = false;
  alg_.unlock(); 
} 

void ArtagPbvsAlgNode::targetStopCallback(void) 
{ 
  alg_.lock(); 
    this->enable_ = false;
    this->init_ = true;
    this->is_in_zone_ = false;    
  alg_.unlock(); 
} 

bool ArtagPbvsAlgNode::targetIsFinishedCallback(void) 
{ 
  bool finish = false; 

  alg_.lock(); 

  if(this->is_in_zone_){
    finish = true;
    this->enable_=false;
    this->init_ = true;
  } 

  alg_.unlock(); 

  return finish; 
} 

bool ArtagPbvsAlgNode::targetHasSucceedCallback(void) 
{ 
  bool ret = true; 

  // alg_.lock(); 

  //   ret = true;

  // alg_.unlock(); 

  return ret; 
} 

void ArtagPbvsAlgNode::targetGetResultCallback(iri_visual_servo_msgs::vs_targetResultPtr& result) 
{ 
  alg_.lock(); 

    //update result data to be sent to client 
    result->goalID = this->fixed_to_id_;

  alg_.unlock(); 
} 

void ArtagPbvsAlgNode::targetGetFeedbackCallback(iri_visual_servo_msgs::vs_targetFeedbackPtr& feedback) 
{ 
  alg_.lock(); 

    //keep track of feedback 
    feedback->busy = true;
    feedback->dist_linear_to_goal =  this->curr_dist_error_;
    feedback->dist_angular_to_goal = this->curr_angle_error_;

  alg_.unlock(); 
}

/*  [action requests] */

//! Get values form dynamic reconfigure
void ArtagPbvsAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();

  // PID parameters
  this->kp_(0,0) = config.kp_x;
  this->kd_(0,0) = config.kd_x;
  this->ki_(0,0) = config.ki_x;
  this->kp_(1,0) = config.kp_y;
  this->kd_(1,0) = config.kd_y;
  this->ki_(1,0) = config.ki_y;
  this->kp_(2,0) = config.kp_z;
  this->kd_(2,0) = config.kd_z;
  this->ki_(2,0) = config.ki_z;
  this->kp_(3,0) = config.kp_roll;
  this->kd_(3,0) = config.kd_roll;
  this->ki_(3,0) = config.ki_roll;
  this->kp_(4,0) = config.kp_pitch;
  this->kd_(4,0) = config.kd_pitch;
  this->ki_(4,0) = config.ki_pitch;
  this->kp_(5,0) = config.kp_yaw;
  this->kd_(5,0) = config.kd_yaw;
  this->ki_(5,0) = config.ki_yaw;
  this->i_lim_ = config.i_lim;

  this->alg_.unlock();
}

//! Add diagnostics
void ArtagPbvsAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
//! Main function
int main(int argc,char *argv[])
{
  return algorithm_base::main<ArtagPbvsAlgNode>(argc, argv, "artag_pbvs_alg_node");
}
