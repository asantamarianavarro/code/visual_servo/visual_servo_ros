#include "artag_pbvs_alg.h"

using namespace Eigen;
using namespace std;

ArtagPbvsAlgorithm::ArtagPbvsAlgorithm(void)
{
}

ArtagPbvsAlgorithm::~ArtagPbvsAlgorithm(void)
{
}

void ArtagPbvsAlgorithm::config_update(Config& new_cfg, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=new_cfg;
  
  this->unlock();
}

// ArtagPbvsAlgorithm Public API

//! Pose-Based Visual Servo
void ArtagPbvsAlgorithm::pose_based_vs(const MatrixXd& desired_pose,
                                     const MatrixXd& current_pose,
                                     const double& dt, 
                                     const MatrixXd& kp,
                                     const MatrixXd& kd,
                                     const MatrixXd& ki,
                                     const double& i_lim,
                                     const bool& write_files,
                                     const string& folder_name,
                                     const double& ts,
                                     MatrixXd& error,
                                     MatrixXd& cam_vel,
                                     std::vector<float>& debug_data)
{

  this->pbvs_.pose_based_vs(desired_pose, current_pose, dt, kp, kd, ki, i_lim, error, cam_vel);

  // Store debug data
  debug_data.clear();

  debug_data.push_back(ts);
  
  // camera velocity
  for (int ii = 0; ii < cam_vel.rows(); ++ii)
    debug_data.push_back(cam_vel(ii,0));
  // pose error
  for (int ii = 0; ii < error.rows(); ++ii)
    debug_data.push_back(error(ii,0));

  if (write_files)
  {
    this->dbg_tools_.write_to_file(folder_name,"cam_vel.txt",cam_vel,ts);
    this->dbg_tools_.write_to_file(folder_name,"pose_error.txt",error,ts);
  }
}