#include "artag_ibvs_alg_node.h"

using namespace Eigen;
using namespace std;

//! Class constructor
ArtagIbvsAlgNode::ArtagIbvsAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<ArtagIbvsAlgorithm>(),
  target_aserver_(public_node_handle_, "target")
{
  //init class attributes if necessary
  //this->loop_rate_ = 100;//in [Hz]

  this->curr_t_ = 0;

  this->time_ini_ = ros::Time::now();

  this->enable_=false;
  this->marker_ok_ = false;
  this->cov_ = 100;
  this->fixed_to_id_ = 0;
  this->is_in_zone_=false;
  this->is_in_dist_zone_ = false;
  this->is_in_angular_zone_ = false;

  this->MarkerArray_msg_.markers.reserve(100);

  // Image attributes
  this->public_node_handle_.param<int>("width", this->img_width_, 1280);
  this->public_node_handle_.param<int>("height", this->img_height_, 960);

  this->public_node_handle_.param<bool>("write_files_", this->write_files_, false);
  this->public_node_handle_.param<string>("folder_name_", this->folder_name_, "/Desktop/Results");

  //Initialize vectors
  this->current_pose_ = MatrixXd::Zero(6,1);
  this->desired_pose_ = MatrixXd::Zero(6,1);
  this->kp_ = MatrixXd::Zero(6,1);
  this->kd_ = MatrixXd::Zero(6,1);
  this->ki_ = MatrixXd::Zero(6,1);
  this->cam_vel_ = MatrixXd::Zero(6,1);
  this->error_ = MatrixXd::Zero(6,1);
  this->max_confidence_error_ = MatrixXd::Zero(2,1);

  // Image-Based parameters
  this->traditional_ = false;
  this->underact_ = false;
  this->random_points_ = false;
  this->init_=true; 

  // Get desired position from launch file
  double x,y,z,roll_tmp,roll,pitch,yaw;
  this->public_node_handle_.param<double>("desired_x", x, 0);
  this->public_node_handle_.param<double>("desired_y", y, 0);
  this->public_node_handle_.param<double>("desired_z", z, 0);
  this->public_node_handle_.param<double>("desired_roll", roll_tmp, 0);
  this->public_node_handle_.param<double>("desired_pitch", pitch, 0);
  this->public_node_handle_.param<double>("desired_yaw", yaw, 0);
  roll = roll_tmp-3.14159265359;

  // Confidence zone
  this->public_node_handle_.param<double>("confidence_linear", this->max_confidence_error_(0,0), 0);
  this->public_node_handle_.param<double>("confidence_angular", this->max_confidence_error_(1,0), 0);

  // High value initialization (robot far from goal)
  this->curr_dist_error_ = 100;
  this->curr_angle_error_ = 100;
  this->first_dist_ = this->curr_dist_error_;
  this->first_time_ = false;
  this->first_tag_ = false;

  this->desired_pose_(0,0) = x;
  this->desired_pose_(1,0) = y;
  this->desired_pose_(2,0) = z;
  // Substract pi because the camera should be looking at the marker
  this->desired_pose_(3,0) = pi2pi(roll); 
  this->desired_pose_(4,0) = pi2pi(pitch);
  this->desired_pose_(5,0) = pi2pi(yaw);

  // [init publishers]
  this->debug_data_publisher_ = this->public_node_handle_.advertise<std_msgs::Float64MultiArray>("debug_data", 1);
  this->marker_array_publisher_ = this->public_node_handle_.advertise<visualization_msgs::MarkerArray>("marker_array", 10);
  this->cmd_vel_cov_publisher_ = this->public_node_handle_.advertise<geometry_msgs::TwistWithCovariance>("cmd_vel_cov", 10);
  
  // [init subscribers]
  this->odom_subscriber_ = this->public_node_handle_.subscribe("odom", 10, &ArtagIbvsAlgNode::odom_callback, this);
  this->input_ARtag_subscriber_ = this->public_node_handle_.subscribe("input_ARtag", 10, &ArtagIbvsAlgNode::input_ARtag_callback, this);
  
  // [init services]
  
  // [init clients]
  
  // [init action servers]
  target_aserver_.registerStartCallback(boost::bind(&ArtagIbvsAlgNode::targetStartCallback, this, _1)); 
  target_aserver_.registerStopCallback(boost::bind(&ArtagIbvsAlgNode::targetStopCallback, this)); 
  target_aserver_.registerIsFinishedCallback(boost::bind(&ArtagIbvsAlgNode::targetIsFinishedCallback, this)); 
  target_aserver_.registerHasSucceedCallback(boost::bind(&ArtagIbvsAlgNode::targetHasSucceedCallback, this)); 
  target_aserver_.registerGetResultCallback(boost::bind(&ArtagIbvsAlgNode::targetGetResultCallback, this, _1)); 
  target_aserver_.registerGetFeedbackCallback(boost::bind(&ArtagIbvsAlgNode::targetGetFeedbackCallback, this, _1)); 
  target_aserver_.start();
  
  // [init action clients]
}

//! Class destructor
ArtagIbvsAlgNode::~ArtagIbvsAlgNode(void)
{
  // [free dynamic memory]
}

//! Main node thread
void ArtagIbvsAlgNode::mainNodeThread(void)
{

  // [fill msg structures]
  // Initialize the topic message structure
  //this->debug_data_msg_.data = my_var;


  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]

  // [publish messages]
}

/*  [subscriber callbacks] */
//! Input Odometry callback
void ArtagIbvsAlgNode::odom_callback(const nav_msgs::Odometry::ConstPtr& msg) 
{ 
  //ROS_INFO("ArtagIbvsAlgNode::odom_callback: New Message Received"); 

  //use appropiate mutex to shared variables if necessary 
  this->odom_mutex_.enter(); 

  // In the case of a underactuation
  // Extract the Non controllable DOF velocities
  if (msg->twist.covariance[0]<100)
  {

    this->v_rollpitch_=MatrixXd::Zero(2,1);

    MatrixXd v_rollpitch(2,1);
    v_rollpitch(0,0) = msg->twist.twist.angular.x;
    v_rollpitch(1,0) = msg->twist.twist.angular.y;

    if (this->underact_ && !isnan(v_rollpitch(0,0)) && !isnan(v_rollpitch(1,0))){
      this->v_rollpitch_(0,0) = v_rollpitch(0,0);
      this->v_rollpitch_(1,0) = v_rollpitch(1,0); 
    }
  }   

  //unlock previously blocked shared variables 
  this->odom_mutex_.exit();  
}

//! Input ARtag callback
void ArtagIbvsAlgNode::input_ARtag_callback(const ar_tools_msgs::ARMarkers::ConstPtr& msg) 
{ 
  //ROS_INFO("ArtagIbvsAlgNode::input_ARtag_callback: New Message Received"); 

  //use appropiate mutex to shared variables if necessary 
  this->input_ARtag_mutex_.enter(); 

  this->cam_vel_ = MatrixXd::Zero(6,1);
 
  if (msg->markers.empty()){
  
    this->marker_ok_ = false;

    //Covariance matrix set to high values because there is no marker
    this->TwistWithCovariance_msg_.covariance[0]  = 
    this->TwistWithCovariance_msg_.covariance[7]  = 
    this->TwistWithCovariance_msg_.covariance[14] = 
    this->TwistWithCovariance_msg_.covariance[21] = 
    this->TwistWithCovariance_msg_.covariance[28] = 
    this->TwistWithCovariance_msg_.covariance[35] = 99999;

  }
  else if (!msg->markers.empty() && this->enable_){
  
    this->marker_ok_ = true;

    this->time_last_ = this->time_;

    this->alg_.lock();      

    // markers check
    if ((!isnan(msg->markers[0].pose.pose.orientation.x) && isfinite(msg->markers[0].pose.pose.orientation.x)) || (!isnan(msg->markers[1].pose.pose.orientation.x) && isfinite(msg->markers[1].pose.pose.orientation.x)))
    {
      // Get marker info
      get_marker_info(msg);

      this->cov_ = double(100-msg->markers.back().confidence)/100;
    }
    else ROS_ERROR("Marker Reading error");

    this->alg_.unlock();

    // Avoid detection ambiguities with z axis check  
    if (this->current_pose_(2,0) > 0)
    {
      // this->curr_t_ = this->curr_t_ + this->dt_;
      this->curr_t_ = (ros::Time::now() - this->time_ini_).toSec();

      // Image-Based Visual Servo algorithm  
      this->artag_ibvs_alg_.artag_image_based_vs(
        this->img_width_,
        this->img_height_,
        this->traditional_,
        this->random_points_, this->desired_pose_,
        this->current_pose_,this->dt_,this->v_rollpitch_,
        this->underact_,this->kp_,this->kd_,
        this->ki_,this->i_lim_,
        this->write_files_,this->folder_name_,
        this->curr_t_,this->cam_vel_,this->debug_data_);

      // Publish debug data
      this->debug_data_msg_.layout.dim.resize(1);
      this->debug_data_msg_.layout.dim[0].label = "";
      this->debug_data_msg_.layout.dim[0].size = this->debug_data_.size();
      this->debug_data_msg_.layout.dim[0].stride = this->debug_data_.size();
      this->debug_data_msg_.layout.data_offset = 0;

      this->debug_data_msg_.data.resize(this->debug_data_.size());
      for (int ii = 0; ii < this->debug_data_.size(); ++ii)
        this->debug_data_msg_.data[ii] = this->debug_data_.at(ii);
      this->debug_data_publisher_.publish(this->debug_data_msg_);
      
      // Get virtual features to publish them as a markers
      this->artag_ibvs_alg_.get_image_based_features(this->features_,this->base_);
    }  
  
    //Covariance matrix set to low values because there is marker, and 
    this->TwistWithCovariance_msg_.covariance[0]  = 
    this->TwistWithCovariance_msg_.covariance[7]  = 
    this->TwistWithCovariance_msg_.covariance[14] = 
    this->TwistWithCovariance_msg_.covariance[21] = 
    this->TwistWithCovariance_msg_.covariance[28] = 
    this->TwistWithCovariance_msg_.covariance[35] = this->cov_;

  }

  this->first_tag_ = true;

  // Check if finished
  inside_confidence_zone();

  // Fill marker
  fill_marker_array();


  //Twist message to publish with the camera velocities
  this->TwistWithCovariance_msg_.twist.linear.x=this->cam_vel_(0,0);
  this->TwistWithCovariance_msg_.twist.linear.y=this->cam_vel_(1,0);
  this->TwistWithCovariance_msg_.twist.linear.z=this->cam_vel_(2,0);
  this->TwistWithCovariance_msg_.twist.angular.x=this->cam_vel_(3,0);
  this->TwistWithCovariance_msg_.twist.angular.y=this->cam_vel_(4,0);
  this->TwistWithCovariance_msg_.twist.angular.z=this->cam_vel_(5,0);
  
  this->cmd_vel_cov_publisher_.publish(this->TwistWithCovariance_msg_);

  //unlock previously blocked shared variables 
  this->input_ARtag_mutex_.exit();
}

//! Set angles between -pi and pi
void ArtagIbvsAlgNode::inside_confidence_zone()
{
  double x,y,z,roll,pitch,yaw;

  this->is_in_zone_ = false;
  this->is_in_dist_zone_ = false;
  this->is_in_angular_zone_ = false;

  x = this->desired_pose_(0,0)-this->current_pose_(0,0);
  y = this->desired_pose_(1,0)-this->current_pose_(1,0);
  z = this->desired_pose_(2,0)-this->current_pose_(2,0);
  roll = this->desired_pose_(3,0)-this->current_pose_(3,0);
  pitch = this->desired_pose_(4,0)-this->current_pose_(4,0);
  yaw = this->desired_pose_(5,0)-this->current_pose_(5,0);

  roll = pi2pi(roll);
  pitch = pi2pi(pitch);
  yaw = pi2pi(yaw);

  this->curr_dist_error_ = sqrt(x*x+y*y+z*z);
  double angle_err = sqrt(roll*roll+pitch*pitch+yaw*yaw);
  this->curr_angle_error_ = abs(pi2pi(angle_err));

  //DEBUG
  //std::cout << "--------" << std::endl;
  //std::cout << "LIN max: " << this->max_confidence_error_(0,0) << "           current: " << this->curr_dist_error_ << std::endl;
  //std::cout << "ANG max: " << this->max_confidence_error_(1,0) << "           current: " << this->curr_angle_error_ << std::endl;

  if(this->curr_dist_error_ < this->max_confidence_error_(0,0))
    this->is_in_dist_zone_=true;

  if(this->curr_angle_error_ < this->max_confidence_error_(1,0))
    this->is_in_angular_zone_=true;

  if(this->is_in_dist_zone_ && this->is_in_angular_zone_)
    this->is_in_zone_ = true;
}

//! Get Marker info
void ArtagIbvsAlgNode::get_marker_info(const ar_tools_msgs::ARMarkers::ConstPtr& msg)
{

  this->pose_ = msg->markers.back().pose.pose;
  this->time_ = msg->markers.back().header.stamp;
  this->frame_id_ = msg->markers.back().header.frame_id;


  //reset time that will reset the algorithm integration
  if (this->init_)
  {
    this->time_last_=this->time_;
    this->init_=false;
  }
  this->dt_=(this->time_-this->time_last_).toSec();  

  // Get pose
  tf::Transform tf_pose;
  Matrix3d R;
  double roll,pitch,yaw;
  tf::poseMsgToTF(this->pose_, tf_pose);
  tf_pose.getBasis().getRPY(roll,pitch,yaw);

  this->current_pose_(0,0) = this->pose_.position.x; 
  this->current_pose_(1,0) = this->pose_.position.y; 
  this->current_pose_(2,0) = this->pose_.position.z;
  this->current_pose_(3,0) = roll;
  this->current_pose_(4,0) = pitch;
  this->current_pose_(5,0) = yaw;
}

//! Set angles between -pi and pi
double ArtagIbvsAlgNode::pi2pi(double& angle)
{
  double pi = 3.14159265359;
  while (angle >= pi) angle = angle-2*pi;
  while (angle <= -pi) angle = angle+2*pi;
  return angle;
}

void ArtagIbvsAlgNode::fill_marker_array()
{

  if(this->first_tag_)
  {

    if (this->first_time_)
    {
      this->first_dist_ = this->curr_dist_error_;
      this->first_time_ = false;
    }

    this->MarkerArray_msg_.markers.clear();

    visualization_msgs::Marker toroid_marker,m_dist_text,m_angle_text,m_goal;

    m_goal = fill_goal_marker();
    this->MarkerArray_msg_.markers.push_back(m_goal);

    // toroid_marker = fill_toroid_marker();
    // this->MarkerArray_msg_.markers.push_back(toroid_marker);

    m_dist_text = fill_m_dist_text();
    this->MarkerArray_msg_.markers.push_back(m_dist_text);

    m_angle_text = fill_m_angle_text();
    this->MarkerArray_msg_.markers.push_back(m_angle_text);

    visualization_msgs::MarkerArray m_features,m_base;

    m_features = fill_m_features();
    for (unsigned int ii = 0; ii < m_features.markers.size(); ++ii)
    {
      this->MarkerArray_msg_.markers.push_back(m_features.markers[ii]);
    }

    m_base = fill_m_base();
    for (unsigned int ii = 0; ii < m_base.markers.size(); ++ii)
    {
      this->MarkerArray_msg_.markers.push_back(m_base.markers[ii]);
    }    

    this->marker_array_publisher_.publish(this->MarkerArray_msg_);
  }
}

visualization_msgs::Marker ArtagIbvsAlgNode::fill_goal_marker()
{
 
  double red = (this->curr_dist_error_/this->first_dist_);
  double green = 1-red;

  // Torid Marker (mesh resource)
	visualization_msgs::Marker current_marker;
  current_marker.header.frame_id = this->frame_id_;
  current_marker.header.stamp = this->time_;
  current_marker.ns = "basic_shapes";
  current_marker.type = visualization_msgs::Marker::SPHERE;
  current_marker.action = visualization_msgs::Marker::ADD;
  // current_marker.lifetime = ros::Duration(2*this->dt_);
  current_marker.id = 0;

  Matrix4d HT_target = get_HT(this->current_pose_);
  Matrix4d HT_desired = get_HT(this->desired_pose_);
  Matrix4d HT_desired_in_cam = HT_target * HT_desired;
  current_marker.pose.position.x = HT_desired_in_cam(0,3);
  current_marker.pose.position.y = HT_desired_in_cam(1,3);
  current_marker.pose.position.z = HT_desired_in_cam(2,3);
  current_marker.pose.orientation.x = 0.0;
  current_marker.pose.orientation.y = 0.0;
  current_marker.pose.orientation.z = 0.0;
  current_marker.pose.orientation.w = 1.0;  
  current_marker.scale.x = this->max_confidence_error_(0,0);
  current_marker.scale.y = this->max_confidence_error_(0,0);
  current_marker.scale.z = this->max_confidence_error_(0,0);

  current_marker.color.a = 1.0;
  if (this->is_in_zone_) current_marker.color.a = 0.25; 
  current_marker.color.r = (float)red;
  current_marker.color.g = (float)green;
  current_marker.color.b = 0.0f;

  return current_marker;
}

// visualization_msgs::Marker ArtagIbvsAlgNode::fill_toroid_marker()
// {
//   // Torid Marker (mesh resource)
//   visualization_msgs::Marker current_marker;
//   double red = (this->curr_dist_error_/this->first_dist_);
//   double green = 1-red;    

//   current_marker.header.frame_id = this->frame_id_;
//   current_marker.header.stamp = this->time_;
//   current_marker.ns = "basic_shapes"; 
//   current_marker.type = visualization_msgs::Marker::MESH_RESOURCE;
//   current_marker.mesh_resource = "package://iri_artag_ibvs/media/models/target_marker.stl";
//   current_marker.action = visualization_msgs::Marker::ADD;
//   current_marker.lifetime = ros::Duration(2*this->dt_);
//   current_marker.id = 0;
//   current_marker.pose = this->pose_;
//   current_marker.scale.x = 0.075;
//   current_marker.scale.y = 0.075;
//   current_marker.scale.z = 0.075;
//   current_marker.color.a = 1.0;

//   if (this->is_in_zone_) current_marker.color.a = 0.25; 
//   current_marker.color.r = (float)red;
//   current_marker.color.g = (float)green;
//   current_marker.color.b = 0.0f;

//   return current_marker;
// }

visualization_msgs::Marker ArtagIbvsAlgNode::fill_m_dist_text()
{
  // Text marker Linear Euclidean Distance
  visualization_msgs::Marker m_dist_text;
  m_dist_text.header.frame_id = this->frame_id_;
  m_dist_text.header.stamp = this->time_;
  m_dist_text.ns = "basic_shapes";
  m_dist_text.id = 1;
  m_dist_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  m_dist_text.action = visualization_msgs::Marker::ADD;
  m_dist_text.pose = this->pose_;
  m_dist_text.pose.position.x += 0.5;
  m_dist_text.pose.position.y += 0.5;
  m_dist_text.scale.x = 0;
  m_dist_text.scale.y = 0;
  m_dist_text.scale.z = 0.25;
  m_dist_text.color.a = 1.0;
  if (this->is_in_dist_zone_)
  {
    m_dist_text.color.r = 0.0;
    m_dist_text.color.g = 1.0;
    m_dist_text.color.b = 0.0;
  }
  else 
  {
    m_dist_text.color.r = 1.0;
    m_dist_text.color.g = 0.0;
    m_dist_text.color.b = 0.0;      
  }
  ostringstream dist_strs;
  dist_strs << "L:";
  dist_strs << this->curr_dist_error_;
  string dist_text = dist_strs.str();
  m_dist_text.text=dist_text;

  return m_dist_text;
}

visualization_msgs::Marker ArtagIbvsAlgNode::fill_m_angle_text()
{
  // Text marker Linear Euclidean Distance
  visualization_msgs::Marker m_angle_text;
  m_angle_text.header.frame_id = this->frame_id_;
  m_angle_text.header.stamp = this->time_;
  m_angle_text.ns = "basic_shapes";
  m_angle_text.id = 2;
  m_angle_text.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
  m_angle_text.action = visualization_msgs::Marker::ADD;
  m_angle_text.pose = this->pose_;
  m_angle_text.pose.position.x += 0.7;
  m_angle_text.pose.position.y += 0.7;
  m_angle_text.scale.x = 0;
  m_angle_text.scale.y = 0;
  m_angle_text.scale.z = 0.25;
  m_angle_text.color.a = 1.0;
  if (this->is_in_angular_zone_)
  {
    m_angle_text.color.r = 0.0;
    m_angle_text.color.g = 1.0;
    m_angle_text.color.b = 0.0;
  }
  else 
  {
    m_angle_text.color.r = 1.0;
    m_angle_text.color.g = 0.0;
    m_angle_text.color.b = 0.0;      
  }
  ostringstream angular_strs;
  angular_strs << "A:";
  angular_strs << this->curr_angle_error_;
  string angle_text = angular_strs.str();
  m_angle_text.text=angle_text;  

  return m_angle_text;
}

visualization_msgs::MarkerArray ArtagIbvsAlgNode::fill_m_features()
{
  visualization_msgs::MarkerArray m_features;

  visualization_msgs::Marker marker;
  marker.header.frame_id = this->frame_id_;
  marker.header.stamp = this->time_;
  marker.ns = "basic_shapes";
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  // marker.pose = this->pose_;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.05;
  marker.scale.y = 0.05;
  marker.scale.z = 0.05;
  marker.color.a = 1.0;
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.75;

  Matrix4d HT_target = get_HT(this->current_pose_);
  MatrixXd point;
  for (int ii = 0; ii < this->features_.cols(); ++ii)
  {
    point = HT_target * this->features_.col(ii);

    marker.pose.position.x = point(0,0);
    marker.pose.position.y = point(1,0);
    marker.pose.position.z = point(2,0);
    marker.id = 3+ii;
    m_features.markers.push_back(marker);
  }

  return m_features;
}

visualization_msgs::MarkerArray ArtagIbvsAlgNode::fill_m_base()
{
  visualization_msgs::MarkerArray m_base;
 
  visualization_msgs::Marker marker;
  marker.header.frame_id = this->frame_id_;
  marker.header.stamp = this->time_;
  marker.ns = "basic_shapes";
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  // marker.pose = this->pose_;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;  
  marker.scale.x = 0.05;
  marker.scale.y = 0.05;
  marker.scale.z = 0.05;
  marker.color.a = 1.0;
  marker.color.r = 0.75;
  marker.color.g = 0.0;
  marker.color.b = 1.0;

  Matrix4d HT_target = get_HT(this->current_pose_);
  MatrixXd point;
  for (int ii = 0; ii < this->base_.cols(); ++ii)
  {
    point = HT_target * this->base_.col(ii);
    marker.pose.position.x = point(0,0);
    marker.pose.position.y = point(1,0);
    marker.pose.position.z = point(2,0);
    marker.id = 3+this->features_.cols()+ii;
    m_base.markers.push_back(marker);
  }
  return m_base;
}

Matrix4d ArtagIbvsAlgNode::get_HT(const MatrixXd& pose)
{
  // euler convention zyx
  Eigen::Matrix3d Rot; 
  Rot = Eigen::AngleAxisd(pose(5,0), Eigen::Vector3d::UnitZ()) \
    * Eigen::AngleAxisd(pose(4,0), Eigen::Vector3d::UnitY()) \
    * Eigen::AngleAxisd(pose(3,0), Eigen::Vector3d::UnitX());
   
  Eigen::Matrix4d HT(4,4);
  HT.block(0,0,3,3)=Rot;  
  HT.block(0,3,3,1) << pose(0,0), pose(1,0), pose(2,0);
  HT.row(3) << 0.0, 0.0, 0.0, 1.0;

  return HT;
}


/*  [service callbacks] */

/*  [action callbacks] */
void ArtagIbvsAlgNode::targetStartCallback(const iri_visual_servo_msgs::vs_targetGoalConstPtr& goal)
{ 
  alg_.lock();

    //check goal 
    this->enable_ = true;
    this->init_ = true;
    this->is_in_zone_ = false;

    this->fixed_to_id_ = (double)goal->id;
    for (int ii = 0; ii < 6; ++ii)
    {
      this->desired_pose_(ii,0) = goal->desired_pose[ii];
    }
    for (int ii = 0; ii < 2; ++ii)
    {
      this->max_confidence_error_(ii,0) = goal->max_confidence_error[ii];
    }

    //DEBUG
    //std::cout << this->max_confidence_error_ << std::endl;

    this->first_time_ = true;
    this->first_tag_ = false;

  alg_.unlock(); 
} 

void ArtagIbvsAlgNode::targetStopCallback(void) 
{ 
  alg_.lock(); 
    this->enable_ = false;
    this->init_ = true;
    this->is_in_zone_ = false;    
  alg_.unlock(); 
} 

bool ArtagIbvsAlgNode::targetIsFinishedCallback(void) 
{ 
  bool finish = false; 

  alg_.lock(); 

  if(this->is_in_zone_){
    finish = true;
    this->enable_=false;
    this->init_ = true;
  } 

  alg_.unlock(); 

  return finish; 
} 

bool ArtagIbvsAlgNode::targetHasSucceedCallback(void) 
{ 
  bool ret = true; 

  // alg_.lock(); 

  //   ret = true;

  // alg_.unlock(); 

  return ret; 
} 

void ArtagIbvsAlgNode::targetGetResultCallback(iri_visual_servo_msgs::vs_targetResultPtr& result) 
{ 
  alg_.lock(); 

    //update result data to be sent to client 
    result->goalID = this->fixed_to_id_;

  alg_.unlock(); 
} 

void ArtagIbvsAlgNode::targetGetFeedbackCallback(iri_visual_servo_msgs::vs_targetFeedbackPtr& feedback) 
{ 
  alg_.lock(); 

    //keep track of feedback 
    feedback->busy = true;
    feedback->dist_linear_to_goal =  this->curr_dist_error_;
    feedback->dist_angular_to_goal = this->curr_angle_error_;

  alg_.unlock(); 
}

/*  [action requests] */

//! Get values form dynamic reconfigure
void ArtagIbvsAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();

  // PID parameters
  this->kp_(0,0) = config.kp_x;
  this->kd_(0,0) = config.kd_x;
  this->ki_(0,0) = config.ki_x;
  this->kp_(1,0) = config.kp_y;
  this->kd_(1,0) = config.kd_y;
  this->ki_(1,0) = config.ki_y;
  this->kp_(2,0) = config.kp_z;
  this->kd_(2,0) = config.kd_z;
  this->ki_(2,0) = config.ki_z;
  this->kp_(3,0) = config.kp_roll;
  this->kd_(3,0) = config.kd_roll;
  this->ki_(3,0) = config.ki_roll;
  this->kp_(4,0) = config.kp_pitch;
  this->kd_(4,0) = config.kd_pitch;
  this->ki_(4,0) = config.ki_pitch;
  this->kp_(5,0) = config.kp_yaw;
  this->kd_(5,0) = config.kd_yaw;
  this->ki_(5,0) = config.ki_yaw;
  this->i_lim_ = config.i_lim;

  //Image-Based parameters
  this->traditional_ = config.traditional; 
  this->random_points_ = config.random_points;  
  this->underact_ = config.underact;

  this->alg_.unlock();
}

//! Add diagnostics
void ArtagIbvsAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
//! Main function
int main(int argc,char *argv[])
{
  return algorithm_base::main<ArtagIbvsAlgNode>(argc, argv, "artag_ibvs_alg_node");
}
