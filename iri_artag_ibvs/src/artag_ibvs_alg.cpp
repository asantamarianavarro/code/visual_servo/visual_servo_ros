#include "artag_ibvs_alg.h"

using namespace Eigen;
using namespace std;

ArtagIbvsAlgorithm::ArtagIbvsAlgorithm(void)
{
}

ArtagIbvsAlgorithm::~ArtagIbvsAlgorithm(void)
{
}

void ArtagIbvsAlgorithm::config_update(Config& new_cfg, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=new_cfg;
  
  this->unlock();
}

// ArtagIbvsAlgorithm Public API

//! Image-Based Visual Servo (Calibrated and Uncalibrated)
void ArtagIbvsAlgorithm::artag_image_based_vs(
  const int& img_width,
  const int& img_height,
  const bool& traditional,
  const bool& random_points,
	const MatrixXd& desired_pose,
	const MatrixXd& current_pose,
	const double& dt, 
	const MatrixXd& V_rollpitch,
	const bool& quadrotor,
	const MatrixXd& kp,
	const MatrixXd& kd,
	const MatrixXd& ki,
	const double& i_lim,
  const bool& write_files,
  const string& folder_name,
  const double& ts,
	MatrixXd& cam_vel,
  std::vector<float>& debug_data)
{	

  img_attr img_params;
  img_params.width = img_width;
  img_params.height = img_height;

  this->ibvs_.image_based_vs(img_params, traditional, random_points, desired_pose, current_pose, dt, V_rollpitch, quadrotor, kp, kd, ki, i_lim, cam_vel);

  // Store debug data
  debug_data.clear();

  debug_data.push_back(ts);

  // desired pose
  for (int ii = 0; ii < desired_pose.rows(); ++ii)
    debug_data.push_back(desired_pose(ii,0));
  // // current pose
  for (int ii = 0; ii < current_pose.rows(); ++ii)
    debug_data.push_back(current_pose(ii,0)); 
  // cam_vel
  for (int ii = 0; ii < cam_vel.rows(); ++ii)
    debug_data.push_back(cam_vel(ii,0));   
  // features in image plane
  for (int ii = 0; ii < this->ibvs_.feat_imgplane_.rows(); ++ii)
    for (int jj = 0; jj < this->ibvs_.feat_imgplane_.cols(); ++jj)
      debug_data.push_back(this->ibvs_.feat_imgplane_(ii,jj));   
  // desired features in image plane
  for (int ii = 0; ii < this->ibvs_.feat_x_imgplane_.rows(); ++ii)
    for (int jj = 0; jj < this->ibvs_.feat_x_imgplane_.cols(); ++jj)
      debug_data.push_back(this->ibvs_.feat_x_imgplane_(ii,jj));   

  if (write_files)
  {
    this->dbg_tools_.write_to_file(folder_name,"desired_pose.txt",desired_pose,ts);
    this->dbg_tools_.write_to_file(folder_name,"current_pose.txt",current_pose,ts);
    this->dbg_tools_.write_to_file(folder_name,"features_in_obj.txt",this->ibvs_.features_in_obj_,ts);
    this->dbg_tools_.write_to_file(folder_name,"features_base_in_obj_3d.txt",this->ibvs_.base_in_obj_3d_,ts);
    this->dbg_tools_.write_to_file(folder_name,"features_base_in_obj_4d.txt",this->ibvs_.base_in_obj_4d_,ts);
    this->dbg_tools_.write_to_file(folder_name,"features_alfas_in_obj.txt",this->ibvs_.alfas_in_obj_,ts);
    this->dbg_tools_.write_to_file(folder_name,"features_s.txt",this->ibvs_.s_,ts);
    this->dbg_tools_.write_to_file(folder_name,"features_s_x_.txt",this->ibvs_.s_x_,ts);
    this->dbg_tools_.write_to_file(folder_name,"features_diff.txt",this->ibvs_.diff_,ts);
    this->dbg_tools_.write_to_file(folder_name,"cam_vel.txt",cam_vel,ts);
    this->dbg_tools_.write_to_file(folder_name,"feat_imgplane.txt",this->ibvs_.feat_imgplane_,ts);
    this->dbg_tools_.write_to_file(folder_name,"feat_x_imgplane.txt",this->ibvs_.feat_x_imgplane_,ts);
  }
}

//! Pull Virtual Features to print them as markers
void ArtagIbvsAlgorithm::get_image_based_features(MatrixXd& features, MatrixXd& base)
{
  this->ibvs_.get_features(features,base);
}
