// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _artag_ibvs_alg_h_
#define _artag_ibvs_alg_h_

#include <iri_artag_ibvs/ArtagIbvsConfig.h>
#include "mutex.h"

#include <iostream>
#include <vector>

#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>

// Main Image-Based Visual Servoing Library
#include <image_based_vs.h>

// Debug tools library
#include <debug_tools.h>

//include artag_vs_alg main library

using namespace Eigen;
using namespace std;

/**
 * \brief IRI ROS Specific Driver Class
 *
 *
 */
class ArtagIbvsAlgorithm
{
  protected:
   /**
    * \brief define config type
    *
    * Define a Config type with the ArtagIbvsConfig. All driver implementations
    * will then use the same variable type Config.
    */
    CMutex alg_mutex_;

  public:
   /**
    * \brief define config type
    *
    * Define a Config type with the ArtagIbvsConfig. All driver implementations
    * will then use the same variable type Config.
    */
    typedef iri_artag_ibvs::ArtagIbvsConfig Config;

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

   /**
    * \brief constructor
    *
    * In this constructor parameters related to the specific driver can be
    * initalized. Those parameters can be also set in the openDriver() function.
    * Attributes from the main node driver class IriBaseDriver such as loop_rate,
    * may be also overload here.
    */
    ArtagIbvsAlgorithm(void);

   /**
    * \brief Lock Algorithm
    *
    * Locks access to the Algorithm class
    */
    void lock(void) { alg_mutex_.enter(); };

   /**
    * \brief Unlock Algorithm
    *
    * Unlocks access to the Algorithm class
    */
    void unlock(void) { alg_mutex_.exit(); };

   /**
    * \brief Tries Access to Algorithm
    *
    * Tries access to Algorithm
    * 
    * \return true if the lock was adquired, false otherwise
    */
    bool try_enter(void) { return alg_mutex_.try_enter(); };

   /**
    * \brief config update
    *
    * In this function the driver parameters must be updated with the input
    * config variable. Then the new configuration state will be stored in the 
    * Config attribute.
    *
    * \param new_cfg the new driver configuration state
    *
    * \param level level in which the update is taken place
    */
    void config_update(Config& new_cfg, uint32_t level=0);

    // here define all artag_vs_alg interface methods to retrieve and set
    // the driver parameters

   /**
    * \brief Destructor
    *
    * This destructor is called when the object is about to be destroyed.
    *
    */
    ~ArtagIbvsAlgorithm(void);

    /**
    * \brief Main algorithm
    *
    * Image Based Visual Servoing algorithm call.
    *
    */
    void artag_image_based_vs(
      const int& img_width,
      const int& img_height,
      const bool& traditional,
      const bool& random_points,
      const Eigen::MatrixXd& desired_pose,
      const Eigen::MatrixXd& current_pose,
      const double& dt, 
      const Eigen::MatrixXd& V_rollpitch,
      const bool& quadrotor,
      const Eigen::MatrixXd& kp,
      const Eigen::MatrixXd& kd,
      const Eigen::MatrixXd& ki,
      const double& i_lim,
      const bool& write_files,
      const string& folder_name,
      const double& ts,
      Eigen::MatrixXd& cam_vel,
      std::vector<float>& debug_data);

    void get_image_based_features(MatrixXd& features, MatrixXd& base);

    CImage_Based_Vs ibvs_; // Low level Image-Based Visual Servo class
    CDebug_Tools dbg_tools_; // Low level Debug Tools class
};

#endif
